class Post {
  constructor({ postRepository, postReactionRepository }) {
    this._postRepository = postRepository;
    this._postReactionRepository = postReactionRepository;
  }

  getPosts(filter) {
    return this._postRepository.getPosts(filter);
  }

  getPostById(id) {
    return this._postRepository.getPostById(id);
  }

  delete(id) {
    return this._postRepository.updateById(id, { isDeleted: true });
  }

  create(userId, post) {
    return this._postRepository.create({
      ...post,
      userId,
      isDeleted: false
    });
  }

  update(id, properties) {
    return this._postRepository.updateById(id, properties);
  }

  async setReaction(userId, { postId, isLike = true }) {
    // define the callback for future use as a promise

    const result = { likeDiff: 0, dislikeDiff: 0 };

    const updateOrDelete = react => {
      if (react.isLike === isLike) {
        this._postReactionRepository.deleteById(react.id);
        if (isLike) result.likeDiff = -1;
        else result.dislikeDiff = -1;
      } else {
        this._postReactionRepository.updateById(react.id, { isLike });
        if (isLike) {
          result.likeDiff = 1;
          result.dislikeDiff = -1;
        } else {
          result.likeDiff = -1;
          result.dislikeDiff = 1;
        }
      }
    };

    const reaction = await this._postReactionRepository.getPostReaction(
      userId,
      postId
    );

    if (reaction) {
      await updateOrDelete(reaction);
    } else {
      await this._postReactionRepository.create({ userId, postId, isLike });
      if (isLike) {
        result.likeDiff = 1;
      } else {
        result.dislikeDiff = 1;
      }
    }

    return result;
  }
}

export { Post };
