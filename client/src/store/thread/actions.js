import { createAction } from '@reduxjs/toolkit';
import {
  comment as commentService,
  post as postService,
  image as imageService
} from 'src/services/services';

const ActionType = {
  ADD_POST: 'thread/add-post',
  LOAD_MORE_POSTS: 'thread/load-more-posts',
  SET_ALL_POSTS: 'thread/set-all-posts',
  SET_EXPANDED_POST: 'thread/set-expanded-post',
  UPDATE_POST: 'thread/update-post'
};

const setPosts = createAction(ActionType.SET_ALL_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addMorePosts = createAction(ActionType.LOAD_MORE_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addPost = createAction(ActionType.ADD_POST, post => ({
  payload: {
    post
  }
}));

const setExpandedPost = createAction(ActionType.SET_EXPANDED_POST, post => ({
  payload: {
    post
  }
}));

const setUpdatePost = createAction(ActionType.UPDATE_POST, post => ({
  payload: {
    post
  }
}));

const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPosts(posts));
};

const loadMorePosts = filter => async (dispatch, getRootState) => {
  const {
    posts: { posts }
  } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts.filter(
    post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))
  );
  dispatch(addMorePosts(filteredPosts));
};

const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPost(post));
};

const createPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPost(newPost));
};

const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPost(post));
};

const updatedPostToggle = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setUpdatePost(post));
};

const updatePost = (postId, properties) => async (dispatch, getRootState) => {
  const updatedPost = await postService.updatePost(postId, properties);

  const {
    posts: { posts, expandedPost }
  } = getRootState();

  let updatedPostImage;
  if (updatedPost.imageId) {
    updatedPostImage = await imageService.getImage(updatedPost.imageId);
  }

  const mapPost = post => ({
    ...post,
    body: updatedPost.body,
    updatedAt: updatedPost.updatedAt,
    imageId: updatedPost.imageId,
    image: updatedPostImage ? { id: updatedPostImage.id, link: updatedPostImage.link } : null
  });

  const updated = posts.map(post => (post.id !== postId ? post : mapPost(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapPost(expandedPost)));
  }
};

const deletePost = postId => async (dispatch, getRootState) => {
  const deletedPost = await postService.deletePost(postId);

  const {
    posts: { posts, expandedPost }
  } = getRootState();

  const updated = posts.filter(post => post.id !== deletedPost.id);

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost());
  }
};

const likePost = (postId, react) => async (dispatch, getRootState) => {
  const { likeDiff, dislikeDiff } = await postService.likePost(postId, react);

  const mapReactions = post => ({
    ...post,
    likeCount: Number(post.likeCount) + likeDiff,
    dislikeCount: Number(post.dislikeCount) + dislikeDiff
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();

  const updated = posts.map(post => (post.id !== postId ? post : mapReactions(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapReactions(expandedPost)));
  }
};

const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId ? post : mapComments(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

export {
  setPosts,
  addMorePosts,
  addPost,
  setExpandedPost,
  setUpdatePost,
  loadPosts,
  loadMorePosts,
  applyPost,
  createPost,
  toggleExpandedPost,
  likePost,
  addComment,
  updatedPostToggle,
  updatePost,
  deletePost
};
