import * as React from 'react';
import PropTypes from 'prop-types';
import { Modal, Button } from 'src/components/common/common';
import { ButtonColor } from 'src/common/enums/enums';

import styles from './styles.module.scss';

const DeletePost = ({ postId, close, onPostDelete }) => {
  const handleDeletePost = () => {
    onPostDelete(postId);
    close();
  };

  return (
    <Modal
      open
      onClose={close}
    >
      <Modal.Header className={styles.header}>
        <span>Delete Post</span>
      </Modal.Header>
      <Modal.Content className={styles.content}>
        <span>Are you sure you want to delete the post?</span>
        <Button
          color={ButtonColor.BLUE}
          onClick={handleDeletePost}
        >
          Yes
        </Button>
      </Modal.Content>
    </Modal>
  );
};

DeletePost.propTypes = {
  postId: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired,
  onPostDelete: PropTypes.func.isRequired
};

export default DeletePost;
