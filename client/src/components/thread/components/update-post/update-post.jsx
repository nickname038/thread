import * as React from 'react';
import PropTypes from 'prop-types';
import { ButtonColor, ButtonType, IconName } from 'src/common/enums/enums';
import { useSelector } from 'react-redux';
import { Modal, Button, Form, Image, Segment } from 'src/components/common/common';

import styles from './styles.module.scss';

const UpdatePost = ({ onPostUpdate, uploadImage, handleUpdatedPostToggle }) => {
  const { post } = useSelector(state => ({
    post: state.posts.updatedPost
  }));

  const [body, setBody] = React.useState(post.body);
  const [image, setImage] = React.useState(post.image);
  const [isUploading, setIsUploading] = React.useState(false);

  const handleUpdatedPostClose = () => handleUpdatedPostToggle();

  const handleUpdatePost = async () => {
    if (!body) {
      return;
    }

    await onPostUpdate(post.id, { imageId: image?.id, body });
    handleUpdatedPostClose();
  };

  const handleUploadFile = ({ target }) => {
    setIsUploading(true);
    const [file] = target.files;

    uploadImage(file)
      .then(({ id, link }) => {
        setImage({ id, link });
      })
      .catch(() => {
        // TODO: show error
      })
      .finally(() => {
        setIsUploading(false);
      });
  };

  return (
    <Modal open onClose={handleUpdatedPostClose}>
      <Modal.Header className={styles.header}>
        <span>Update Post</span>
      </Modal.Header>
      <Modal.Content>
        <Segment>
          <Form onSubmit={handleUpdatePost}>
            <Form.TextArea
              name="body"
              value={body}
              onChange={ev => setBody(ev.target.value)}
            />
            {image?.link && (
              <div className={styles.imageWrapper}>
                <Image className={styles.image} src={image?.link} alt="post" />
              </div>
            )}
            <div className={styles.btnWrapper}>
              <Button
                color="teal"
                isLoading={isUploading}
                isDisabled={isUploading}
                iconName={IconName.IMAGE}
              >
                <label className={styles.btnImgLabel}>
                  Attach image
                  <input
                    name="image"
                    type="file"
                    onChange={handleUploadFile}
                    hidden
                  />
                </label>
              </Button>
              <Button color={ButtonColor.BLUE} type={ButtonType.SUBMIT}>
                Update
              </Button>
            </div>
          </Form>
        </Segment>
      </Modal.Content>
    </Modal>
  );
};

UpdatePost.propTypes = {
  onPostUpdate: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  handleUpdatedPostToggle: PropTypes.func.isRequired
};

export default UpdatePost;
